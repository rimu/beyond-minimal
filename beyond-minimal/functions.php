<?php
/**
 * Beyond Minimal functions and definitions
 *
 * @package Beyond Minimal
 */

if ( ! function_exists( 'beyond_minimal_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function beyond_minimal_setup() {

	/**
	 * Set the content width based on the theme's design and stylesheet.
	 */
	global $content_width;
	if ( ! isset( $content_width ) ) {
		$content_width = 700;
	}

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Beyond Minimal, use a find and replace
	 * to change 'beyond-minimal' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'beyond-minimal', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 700, 0, false );
	add_image_size( 'beyond-minimal-post-thumbnail-large', 1035, 500, true );
	add_image_size( 'beyond-minimal-post-thumbnail-medium', 482, 300, true );

	// This theme uses wp_nav_menu() in two location.
	register_nav_menus( array(
		'primary'       => esc_html__( 'Main Navigation', 'beyond-minimal' ),
		'footer-social' => esc_html__( 'Footer Social Links', 'beyond-minimal' ),
	) );
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'audio', 'chat', 'image', 'link', 'quote', 'status'
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'header-text' => array( 'site-title', 'site-description' ),
	) );

	// Setup the WordPress core custom header feature.
	add_theme_support( 'custom-header', apply_filters( 'beyond_minimal_custom_header_args', array(
		'default-image' => '',
		'width'         => 1035,
		'height'        => 500,
		'flex-height'   => true,
		'header-text'   => false,
	) ) );

	// This theme styles the visual editor to resemble the theme style.
	add_theme_support( 'editor-styles' );
	add_editor_style( array( 'css/editor-style.css' ) );
}
endif; // beyond_minimal_setup
add_action( 'after_setup_theme', 'beyond_minimal_setup' );

/**
 * Adjust content_width value for full width template.
 */
function beyond_minimal_content_width() {
	if ( is_page_template( 'page_fullwidth.php' ) ) {
		global $content_width;
		$content_width = 1035;
	}
}
add_action( 'template_redirect', 'beyond_minimal_content_width' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function beyond_minimal_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Left', 'beyond-minimal' ),
		'id'            => 'footer-1',
		'description'   => __( 'Footer Left widget area is displayed on the left side of the footer. If you do not use the area, nothing will be displayed.', 'beyond-minimal' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Right 1', 'beyond-minimal' ),
		'id'            => 'footer-2',
		'description'   => __( '3 Footer Right widget areas are displayed on the right side of the footer, and the width is auto-adjusted based on how many you use. If you do not use the area, nothing will be displayed.', 'beyond-minimal' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Right 2', 'beyond-minimal' ),
		'id'            => 'footer-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Right 3', 'beyond-minimal' ),
		'id'            => 'footer-4',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'beyond_minimal_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function beyond_minimal_scripts() {
	wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize.css',  array(), '8.0.0' );
	wp_enqueue_style( 'beyond-minimal-style', get_stylesheet_uri(), array(), '2.1.2' );
	if ( 'ja' == get_bloginfo( 'language' ) ) {
		wp_enqueue_style( 'beyond-minimal-ja', get_template_directory_uri() . '/css/ja.css', array(), null );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	//wp_enqueue_script( 'beyond-minimal-functions', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20190226', true );

	//remove Gutenburg
	wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-blocks-style' );
    wp_dequeue_style( 'global-styles' );
}
add_action( 'wp_enqueue_scripts', 'beyond_minimal_scripts' );

/**
 * Add customizer style to the header.
 */
function beyond_minimal_customizer_css() {
	?>
	<style type="text/css">
		/* Colors */
		<?php if ( $beyond_minimal_link_color = get_theme_mod( 'beyond_minimal_link_color' ) ) : ?>
		.entry-content a, .entry-summary a, .page-content a, .home-text a, .author-profile-description a, .comment-content a {
			color: <?php echo esc_attr( $beyond_minimal_link_color ); ?>;
		}
		<?php endif; ?>
		<?php if ( $beyond_minimal_link_hover_color = get_theme_mod( 'beyond_minimal_link_hover_color' ) ) : ?>
		a:hover {
			color: <?php echo esc_attr( $beyond_minimal_link_hover_color ); ?>;
		}
		<?php endif; ?>

		<?php if ( get_theme_mod( 'beyond_minimal_home_text' ) ) : ?>
		/* Home Text */
			.home-text {
				<?php if ( $beyond_minimal_home_text_font = get_theme_mod( 'beyond_minimal_home_text_font' ) ) :
					if ( 'Safe Serif' == $beyond_minimal_home_text_font ) {
						$home_text_font_family = "Georgia', serif, '";
						$font_weight = "400";
					} elseif ( 'Safe Sans' == $beyond_minimal_home_text_font ) {
						$home_text_font_family = "Helvetica', 'Arial', sans-serif, '";
						$font_weight = "400";
					} elseif ( 'Japanese Sans' == $beyond_minimal_home_text_font ) {
						$home_text_font_family = "Arial', 'Hiragino Sans', 'Hiragino Kaku Gothic ProN', Meiryo, sans-serif, '";
						$font_weight = "400";
					} elseif ( strpos( $beyond_minimal_home_text_font, ':' ) !== false ) {
						list( $home_text_font_family, $font_weight ) = explode( ":", $beyond_minimal_home_text_font );
					} else {
						$home_text_font_family = $beyond_minimal_home_text_font;
						$font_weight = '400';
					}
				?>
				font-family: '<?php echo esc_attr( $home_text_font_family ); ?>', serif;
				font-weight: <?php echo esc_attr( $font_weight ); ?>;
				<?php endif; ?>
				<?php if ( $beyond_minimal_home_text_font_size = get_theme_mod( 'beyond_minimal_home_text_font_size' ) ) : ?>
				font-size: <?php echo esc_attr( $beyond_minimal_home_text_font_size ); ?>px;
				<?php endif; ?>
			}
			<?php if ( $beyond_minimal_home_text_font_size ) : ?>
			@media screen and (max-width: 782px) {
				.home-text {
					font-size: <?php echo esc_attr( $beyond_minimal_home_text_font_size * 0.85 ); ?>px;
				}
			}
			<?php endif; ?>
		<?php endif; ?>
	</style>
	<?php
}
add_action( 'wp_head', 'beyond_minimal_customizer_css' );

/**
 * Add custom classes to the body.
 */
function beyond_minimal_body_classes( $classes ) {

	if ( ! get_theme_mod( 'beyond_minimal_hide_navigation' ) ) {
		$classes[] = 'drawer';
	}

	$classes[] = 'header-side';
	$classes[] = 'footer-side';

	if ( is_page_template( 'fullwidth.php' ) || is_404() ) {
		$classes[] = 'full-width';
	} else {
		$classes[] = 'no-sidebar';
	}

	global $post;
	if ( is_singular() && has_post_thumbnail( $post->ID ) ) {
		$classes[] = 'large-thumbnail';
		for ( $i = 0 ; $i < count( $classes ); $i++ ){
			if ( 'no-sidebar' == $classes[$i] ){
				unset( $classes[$i] );
			}
		}
	}

	$footer_widgets = 0;
	$footer_widgets_max = 4;
	for( $i = 2; $i <= $footer_widgets_max; $i++ ) {
		if ( is_active_sidebar( 'footer-' . $i ) ) {
				$footer_widgets++;
		}
	}
	$classes[] = 'footer-' . $footer_widgets;

	if ( get_option( 'show_avatars' ) ) {
		$classes[] = 'has-avatars';
	}

	return $classes;
}
add_filter( 'body_class', 'beyond_minimal_body_classes' );

/**
 * Adds a box to the side column on the Page edit screen.
 */
function beyond_minimal_add_meta_box() {
	add_meta_box( 'beyond_minimal_page_title_display', __( 'Page Title Display', 'beyond-minimal' ), 'beyond_minimal_meta_box_callback', 'page', 'side', 'high' );
}
add_action( 'add_meta_boxes', 'beyond_minimal_add_meta_box' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function beyond_minimal_meta_box_callback( $post ) {

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'beyond_minimal_save_meta_box_data', 'beyond_minimal_meta_box_nonce' );

	/*
	 * Use get_post_meta() to retrieve an existing value
	 * from the database and use the value for the form.
	 */
	global $post;
	$value = get_post_meta( $post->ID, 'beyond_minimal_hide_page_title', true );
	$checked = ( $value ) ? ' checked="checked"' : '';

	echo '<label for="beyond_minimal_hide_page_title">';
	echo '<input type="checkbox" id="beyond_minimal_hide_page_title" name="beyond_minimal_hide_page_title" value="1"' . $checked . ' />';
	echo __( 'Hide Page Title', 'beyond-minimal' );
	echo '</label>';
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function beyond_minimal_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['beyond_minimal_meta_box_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['beyond_minimal_meta_box_nonce'], 'beyond_minimal_save_meta_box_data' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( ! current_user_can( 'edit_page', $post_id ) ) {
		return;
	}

	/* OK, it's safe for us to save the data now. */

	// Sanitize user input.
	$my_data = write_sanitize_checkbox( $_POST['beyond_minimal_hide_page_title'] );

	// Update the meta field in the database.
	update_post_meta( $post_id, 'beyond_minimal_hide_page_title', $my_data );
}
//add_action( 'save_post', 'beyond_minimal_save_meta_box_data' );


function beyond_minimal_init() {
    if (!is_admin() && !is_login()) {
    	global $wp_scripts;
    	//print_r($wp_scripts);
    	//exit();
    	//jquery-migrate
    	//hoverIntent
    	//hoverintent-js
    	wp_deregister_script('hoverIntent');
        wp_register_script('hoverIntent', false);
    	wp_deregister_script('hoverintent-js');
        wp_register_script('hoverintent-js', false);

		wp_deregister_script('jquery-migrate');
        wp_register_script('jquery-migrate', false);
        
        wp_deregister_script('jquery');
        wp_register_script('jquery', false);

        //disable emojii JS
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );	
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		
		// Remove emojii from TinyMCE
		add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );

		if ( ! is_user_logged_in() ) {
            wp_deregister_style('dashicons');
		}
		remove_action( 'wp_head', 'wp_resource_hints', 2, 99 );

    }
}
add_action('init', 'beyond_minimal_init');

/**
 * Filter out the tinymce emoji plugin.
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom widgets for this theme.
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
