<?php
/**
 * Write Theme Customizer
 *
 * @package Write
 */

/**
 * Set the Customizer
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function write_customize_register( $wp_customize ) {

	class Write_Read_Me extends WP_Customize_Control {
		public function render_content() {
			?>
			<div class="customize-text">
				<p><?php esc_html_e( 'Thank you for using the Beyond Minimal theme.', 'beyond-minimal' ); ?></p>
				<div class="customize-section first-customize-section">
				<h3><?php esc_html_e( 'Documentation', 'beyond-minimal' ); ?></h3>
				<p><?php esc_html_e( 'For instructions on theme configuration, please see the documentation.', 'beyond-minimal' ); ?></p>
				<p><a href="<?php echo esc_url( __( 'https://webdevsolutions.co.nz/beyond_minimal_demo/', 'beyond-minimal' ) ); ?>" target="_blank"><?php esc_html_e( 'Theme Documentation', 'beyond-minimal' ); ?></a></p>
				</div>
				<div class="customize-section"><h3><?php esc_html_e( 'Support', 'beyond-minimal' ); ?></h3>
				<p><?php esc_html_e( 'If there is something you don\'t understand even after reading the documentation, please use the support forum.', 'beyond-minimal' ); ?></p>
				<p><a href="<?php echo esc_url( __( 'https://wordpress.org/support/theme/write', 'beyond-minimal' ) ); ?>" target="_blank"><?php esc_html_e( 'Support Forum', 'beyond-minimal' ); ?></a></p>
				</div>
				<div class="customize-section"><h3><?php esc_html_e( 'Review', 'beyond-minimal' ); ?></h3>
				<p><?php esc_html_e( 'If you are satisfied with the theme, we would greatly appreciate if you would review it.', 'beyond-minimal' ); ?></p>
				<p><a href="<?php echo esc_url( __( 'https://wordpress.org/support/view/theme-reviews/write?filter=5', 'beyond-minimal' ) ); ?>" target="_blank"><?php esc_html_e( 'Review This Theme', 'beyond-minimal' ); ?></a></p>
				</div>
			</div>
			<?php
		}
	}

	class Write_Upgrade extends WP_Customize_Control {
		public function render_content() {
			?>
			<div class="customize-text">

			</div>
			<?php
		}
	}

	// Site Identity
	$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';

	// READ ME
	$wp_customize->add_section( 'write_read_me', array(
		'title'    => esc_html__( 'READ ME', 'beyond-minimal' ),
		'priority' => 1,
	) );
	$wp_customize->add_setting( 'write_read_me_text', array(
		'default'           => '',
		'sanitize_callback' => 'write_sanitize_checkbox',
	) );
	$wp_customize->add_control( new Write_Read_Me( $wp_customize, 'write_read_me_text', array(
		'section'  => 'write_read_me',
		'priority' => 1,
	) ) );

	// Fonts
	global $write_home_text_font;
	$write_home_text_font = array(
			''                         => esc_html__( 'Default', 'beyond-minimal' ),
			'Safe Serif'               => 'Georgia, serif',
			'Safe Sans'                => 'Helvetica, Arial, sans-serif',
			'Selected Fonts'           => esc_html__( '----- Selected Fonts -----', 'beyond-minimal' ),
			'Slabo 27px:400'           => 'Slabo 27px',
			'PT Serif:400'             => 'PT Serif',
			'Alegreya:400'             => 'Alegreya',
			'Vollkorn:400'             => 'Vollkorn',
			'Crimson Text:400'         => 'Crimson Text',
			'Vesper Libre:400'         => 'Vesper Libre',
			'Halant:400'               => 'Halant',
			'Josefin Slab:400'         => 'Josefin Slab',
			'Source Sans Pro:400'      => 'Source Sans Pro',
			'Source Sans Pro:300'      => 'Source Sans Pro Light',
			'PT Sans:400'              => 'PT Sans',
			'Roboto:400'               => 'Roboto',
			'Roboto:300'               => 'Roboto Light',
			'Fira Sans:400'            => 'Fira Sans',
			'Fira Sans:300'            => 'Fira Sans Light',
			'Josefin Sans:400'         => 'Josefin Sans',
			'Fredericka the Great:400' => 'Fredericka the Great',
	);

	if ('ja' == get_bloginfo( 'language' ) ) {
		$write_home_text_font = array( 'Japanese Sans' => esc_html__( 'Japanese Sans', 'beyond-minimal' ) ) + $write_home_text_font;
	}

	// Colors
	$wp_customize->get_section( 'colors' )->priority     = 35;
	$wp_customize->add_setting( 'write_link_color' , array(
		'default'   => '#a87d28',
		'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'write_link_color', array(
		'label'    => esc_html__( 'Link Color', 'beyond-minimal' ),
		'section'  => 'colors',
		'priority' => 13,
	) ) );
	$wp_customize->add_setting( 'write_link_hover_color' , array(
		'default'           => '#c49029',
		'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'write_link_hover_color', array(
		'label'    => esc_html__( 'Link Hover Color', 'beyond-minimal' ),
		'section'  => 'colors',
		'priority' => 14,
	) ) );

	// Header Image
	$wp_customize->add_setting( 'write_header_display', array(
		'default'           => '',
		'sanitize_callback' => 'write_sanitize_header_display'
	) );
	$wp_customize->add_control( 'write_header_display', array(
		'label'   => esc_html__( 'Header Image Display', 'beyond-minimal' ),
		'section' => 'header_image',
		'type'    => 'radio',
		'choices' => array(
			''         => esc_html__( 'Display on the blog posts index page', 'beyond-minimal' ),
			'page'     => esc_html__( 'Display on all static pages', 'beyond-minimal' ),
			'site'     => esc_html__( 'Display on the whole site', 'beyond-minimal' ),
		),
		'priority' => 20,
	) );

	// Home
	$wp_customize->add_section( 'write_home', array(
		'title'       => __( 'Home', 'beyond-minimal' ),
		'description' => __( 'HTML tags are allowed in the home text.', 'beyond-minimal' ),
		'priority'    => 75,
	) );
	$wp_customize->add_setting( 'write_home_text', array(
		'default'           => '',
		'sanitize_callback' => 'wp_kses_post',
	) );
	$wp_customize->add_control( 'write_home_text', array(
		'label'    => __( 'Home Text', 'beyond-minimal' ),
		'section'  => 'write_home',
		'type'     => 'textarea',
		'priority' => 11,
	) );
	$wp_customize->add_setting( 'write_home_text_font', array(
		'default'           => '',
		'sanitize_callback' => 'write_sanitize_home_text_font',
	) );
	$wp_customize->add_control( 'write_home_text_font', array(
		'label'   => __( 'Font', 'beyond-minimal' ),
		'section' => 'write_home',
		'type'    => 'select',
		'choices' => $write_home_text_font,
		'priority' => 12,
	) );
	$wp_customize->add_setting( 'write_home_text_font_size', array(
		'default'           => ( 'ja' == get_bloginfo( 'language' ) ) ? '27' : '32',
		'sanitize_callback' => 'beyond_minimal_sanitize_home_text_font_size',
	) );
	$wp_customize->add_control( 'write_home_text_font_size', array(
		'label'    => __( 'Font Size (px)', 'beyond-minimal' ),
		'section'  => 'write_home',
		'type'     => 'text',
		'priority' => 13,
	));
	$wp_customize->add_setting( 'write_home_text_display', array(
		'default'           => '',
		'sanitize_callback' => 'beyond_minimal_sanitize_home_display'
	) );
	$wp_customize->add_control( 'write_home_text_display', array(
		'label'   => esc_html__( 'Home Text Display', 'beyond-minimal' ),
		'section' => 'write_home',
		'type'    => 'radio',
		'choices' => array(
			''          => esc_html__( 'Display on the blog posts index page', 'beyond-minimal' ),
			'front'     => esc_html__( 'Display on the static front page', 'beyond-minimal' ),
			'site'      => esc_html__( 'Display on the whole site', 'beyond-minimal' ),
		),
		'priority' => 14,
	) );

	// Menus
	$wp_customize->add_setting( 'write_hide_navigation', array(
		'default'           => '',
		'sanitize_callback' => 'write_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'write_hide_navigation', array(
		'label'    => esc_html__( 'Hide Main Navigation', 'beyond-minimal' ),
		'section'  => 'menu_locations',
		'type'     => 'checkbox',
		'priority' => 1,
	) );
	$wp_customize->add_setting( 'write_hide_search', array(
		'default'           => '',
		'sanitize_callback' => 'write_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'write_hide_search', array(
		'label'    => esc_html__( 'Hide Search on Main Navigation', 'beyond-minimal' ),
		'section'  => 'menu_locations',
		'type'     => 'checkbox',
		'priority' => 2,
	) );
}
add_action( 'customize_register', 'write_customize_register' );

/**
 * Sanitize user inputs.
 */
function write_sanitize_checkbox( $value ) {
	if ( $value == 1 ) {
		return 1;
	} else {
		return '';
	}
}
function write_sanitize_margin( $value ) {
	if ( preg_match("/^-?[0-9]+$/", $value) ) {
		return $value;
	} else {
		return '0';
	}
}
function write_sanitize_header_display( $value ) {
	$valid = array(
		''         => esc_html__( 'Display on the blog posts index page', 'beyond-minimal' ),
		'page'     => esc_html__( 'Display on all static pages', 'beyond-minimal' ),
		'site'     => esc_html__( 'Display on the whole site', 'beyond-minimal' ),
	);

	if ( array_key_exists( $value, $valid ) ) {
		return $value;
	} else {
		return '';
	}
}
function beyond_minimal_sanitize_home_display( $value ) {
	$valid = array(
		''          => esc_html__( 'Display on the blog posts index page', 'beyond-minimal' ),
		'front'     => esc_html__( 'Display on the static front page', 'beyond-minimal' ),
		'site'      => esc_html__( 'Display on the whole site', 'beyond-minimal' ),
	);

	if ( array_key_exists( $value, $valid ) ) {
		return $value;
	} else {
		return '';
	}
}
function beyond_minimal_sanitize_home_text_font( $value ) {
	global $write_home_text_font;
	unset ( $write_home_text_font['Selected Fonts'] );
	unset ( $write_home_text_font['All Fonts'] );
	$valid = $write_home_text_font;

	if ( array_key_exists( $value, $valid ) ) {
		return $value;
	} else {
		return '';
	}
}
function beyond_minimal_sanitize_home_text_font_size( $value ) {
	if ( preg_match("/^[1-9][0-9]*$/", $value) ) {
		return $value;
	} else {
		return ( 'ja' == get_bloginfo( 'language' ) ) ? '27' : '32';
	}
}


/**
 * Enqueue Customizer CSS
 */
function beyond_minimal_customizer_style() {
	wp_enqueue_style( 'write-customizer-style', get_template_directory_uri() . '/css/customizer.css', array() );
}
add_action( 'customize_controls_print_styles', 'beyond_minimal_customizer_style');
