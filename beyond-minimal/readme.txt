=== Beyond Minimal ===
Contributors: rimu
Tags: small-internet, gemini, gopher, blog
Requires at least: 4.5
Tested up to: 6.0.0
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Beyond Minimal is an extremely minimal WordPress theme inspired by the Small Internet movement.

For more information about Beyond Minimal please go to https://webdevsolutions.co.nz/beyond_minimal_demo/

== Installation ==

1. In the dashboard, go to Appearance -> Themes and click the 'Add New' button.
2. Click the 'Upload Theme' button.
3. Choose 'beyond-minimal.zip' file and click 'Install Now' button.
4. Click the 'Activate' button.
5. Navigate to Appearance > Customize in your admin panel and customize to taste.

== Copyright ==

Beyond Minimal WordPress Theme, Copyright 2014-2022 Rimu Atkinson
Beyond Minimal is distributed under the terms of the GNU GPL

Write is based on Underscores
Underscores is distributed under the terms of the GNU GPL
http://underscores.me/

Beyond Minimal incorporates code from WordPress core
WordPress is distributed under the terms of the GNU GPL
https://wordpress.org/

Beyond Minimal incorporates code from the Write theme
Write is distributed under the terms of the GNU GPL
https://themegraphy.com/wordpress-themes/write/

Beyond Minimal bundles the following third-party resources:

normalize.css, Copyright 2012-2018 Nicolas Gallagher and Jonathan Neal
License: MIT
Source: https://necolas.github.io/normalize.css/

The photo on the screenshot, Copyright 2015 Eric Bailey
License: CC0
Source: https://pixabay.com/en/entrepreneur-startup-start-up-woman-593357/

== Changelog ==

= 0.1 - Jul 25, 2022 =
* Initial release
