# Beyond Minimal

An extremely simple Wordpress theme, inspired by the [Small Internet movement](https://www.linux-magazine.com/Issues/2021/245/The-Rise-of-the-Small-Internet). Ideal for ultimate accessibility, extremely high traffic sites or people on very limited bandwidth or using retro computers. [More background and rationale](https://cheapskatesguide.org/articles/small-internet.html).

* No JavaScript. jQuery and other Wordpress JS has been suppressed.
* Small amount of CSS: ~35 KB.
* No fonts. The default sans-serif font is used.
* No 3rd party scripts, CSS or images.

## Demo

[See demo site](https://webdevsolutions.co.nz/beyond_minimal_demo/)

## Screenshot

![Screenshot](https://webdevsolutions.co.nz/beyond_minimal_demo/screenshot.webp)

